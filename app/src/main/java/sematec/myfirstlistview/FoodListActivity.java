package sematec.myfirstlistview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class FoodListActivity extends AppCompatActivity {
     String ghormesabziURL = "https://www.google.com/search?q=%D9%82%D9%88%D8%B1%D9%85%D9%87+%D8%B3%D8%A8%D8%B2%DB%8C&rlz=1C1CHWL_enIR754IR755&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjqkoGysonZAhXDrFkKHftoDTAQ_AUICigB&biw=1536&bih=735#imgrc=JjAYU6AE57x42M:";
     String cheezburgerURL ="https://www.google.com/search?q=cheezburger&rlz=1C1CHWL_enIR754IR755&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi4nOrusYnZAhXnzVkKHSB0A14Q_AUICigB&biw=1536&bih=735#imgrc=AoTsxXPPcy_voM:";
     String pastaURL = "https://www.google.com/search?q=pasta&rlz=1C1CHWL_enIR754IR755&source=lnms&tbm=isch&sa=X&ved=0ahUKEwj_m4uksInZAhWQtVkKHfnmCD4Q_AUICigB&biw=1536&bih=735#imgrc=W54noNDB9WhYlM:";
     ListView foodslist;
     Context mContext = this;
     @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_food_list );

        foodslist = (ListView) findViewById(R.id.foodsList );

        List<FoodModel> foods = new ArrayList<> (  );

        FoodModel pasta = new FoodModel ();
        pasta.setName ( "‍pasta" );
        pasta.setPrice ( 25000 );
        pasta.setIranian ( false );
        pasta.setImage ( pastaURL );

        foods.add( pasta );

        FoodModel cheezburger = new FoodModel ();
        cheezburger.setName ( "cheezburger" );
        cheezburger.setPrice ( 20000 );
        cheezburger.setIranian ( false );
        cheezburger.setImage ( cheezburgerURL );

        foods.add( cheezburger );

        FoodModel ghormesabzi = new FoodModel ();
        ghormesabzi.setName ( "ghormesabzi" );
        ghormesabzi.setPrice ( 10000 );
        ghormesabzi.setIranian ( true );
        ghormesabzi.setImage ( ghormesabziURL );

        foods.add( ghormesabzi );


        FoddsListAdapter adapter = new FoddsListAdapter (mContext,foods  );
        foodslist.setAdapter (adapter );





    }
}
