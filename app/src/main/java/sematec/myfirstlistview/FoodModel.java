package sematec.myfirstlistview;

/**
 * Created by sabere_pc on 03/02/2018.
 */

public class FoodModel {
    String name;
    int price;
    String resturant;
    Boolean isIranian;
    String more;
    String image;

    public FoodModel() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getResturant() {
        return resturant;
    }

    public void setResturant(String resturant) {
        this.resturant = resturant;
    }

    public Boolean getIranian() {
        return isIranian;
    }

    public void setIranian(Boolean iranian) {
        isIranian = iranian;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

