package sematec.myfirstlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.List;

/**
 * Created by sabere_pc on 03/02/2018.
 */

public class FoddsListAdapter extends BaseAdapter {

    Context mContext;
    List<FoodModel> foods;


    public FoddsListAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size ();
    }

    @Override
    public Object getItem(int i) {
        return foods.get (i );
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
         View v = LayoutInflater.from (mContext).inflate (R.layout.food_list_item,null );

        TextView foodName = v.findViewById (R.id.foodName);
        TextView foodPrice = v.findViewById (R.id.foodPrice);
        ImageView foodImage = v.findViewById (R.id.foodImage);

        foodName.setText (foods.get (i).getName ()  );
        foodPrice.setText (foods.get ( i ).getPrice() +"" );

        Picasso.with(mContext).load(  foods.get(i).getImage() ) .into(foodImage);

        //RequestCreator load = Picasso.with ( mContext ).load ( foods.get ( i ).getImage () ).into( foodImage );


        return v;
    }
}
